import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import math

class LapSRN(nn.Module):
    def __init__(self, conv_f=3, conv_n=64, depth=10, upscale_f = 4, scale = 2, input_channels=3):
        super(LapSRN, self).__init__()
        
        self.f = conv_f
        self.n = conv_n
        self.depth = depth
        self.upscale_f = upscale_f
        self.scale = scale
        self.levels = math.ceil(math.log2(scale))
        self.input_channels=input_channels
        
        # feature extraction branch
        self.opening_conv = nn.Conv2d(self.input_channels, self.n, self.f, padding=self.f//2)
        self.convs = nn.ModuleList()
        self.feature_upsamples = nn.ModuleList()
        self.residual_layers = nn.ModuleList()
        for level in range(self.levels):
            self.convs.append(nn.ModuleList())
            for i in range(self.depth):
                self.convs[level].append(nn.Conv2d(self.n, self.n, self.f, padding=self.f//2))
            self.feature_upsamples.append(nn.ConvTranspose2d(self.n, self.n, self.f, stride=2))
            self.residual_layers.append(nn.Conv2d(self.n, self.input_channels, self.f, padding = self.f//2))
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                m.bias.data.zero_()
                       
        # image reconstruction branch
        self.image_upsamples = nn.ModuleList()
        for level in range(self.levels):
            image_upsample = nn.ConvTranspose2d(self.input_channels, self.input_channels, self.upscale_f, stride=2, padding=1)
            image_upsample.weight.data = self.bilinear_kernel(4, self.input_channels, self.input_channels).float()
            self.image_upsamples.append(image_upsample)
            
            
    def forward(self, x):
        features = F.leaky_relu(self.opening_conv(x), negative_slope=0.2)
        residuals = None
        current_image = x
        image_reconstructions = []
        for level in range(self.levels):
            for layer in range(self.depth):
                features = F.leaky_relu(self.convs[level][layer](features), negative_slope=0.2)
            features = F.leaky_relu(F.pad(self.feature_upsamples[level](features), (0, -(self.f%2), 0, -(self.f%2))), negative_slope=0.2)
            residuals = self.residual_layers[level](features)
            current_image = self.image_upsamples[level](current_image) + residuals
            image_reconstructions.append(current_image)
        
        return tuple(image_reconstructions)
    
    def bilinear_kernel(self, k, inputs, outputs):
        r = k // 2
        if k%2==1:
            center=r
        else:
            center=r+0.5

        c=np.arange(1,k+1)
        kernel_1d = np.ones((1,k)) - np.abs(c-center) / r
        kernel_2d = kernel_1d.T * kernel_1d
        return torch.from_numpy(np.tile(kernel_2d, (inputs, outputs, 1, 1)))
    
    def init_from_mat(self, mat):
        if self.input_channels != 1:
            raise Exception("bluh")
        self.opening_conv.weight.data = torch.from_numpy(mat['input_conv_f']).permute(3,2,0,1).float()
        self.opening_conv.bias.data = torch.from_numpy(mat['input_conv_b']).view(-1).float()
        for level in range(self.levels):
            for i in range(self.depth):
                self.convs[level][i].weight.data = torch.from_numpy(
                    mat['level{0}_conv{1}_f'.format(level+1, i+1)]).permute(3,2,0,1)
                self.convs[level][i].bias.data = torch.from_numpy(
                    mat['level{0}_conv{1}_b'.format(level+1, i+1)]).view(-1)
            self.feature_upsamples[level].weight.data = torch.from_numpy(
                mat['level{0}_upconv_f'.format(level+1)]).permute(3,2,0,1)
            self.feature_upsamples[level].bias.data = torch.from_numpy(
                mat['level{0}_upconv_b'.format(level+1)]).view(-1)
            self.residual_layers[level].weight.data = torch.from_numpy(
                mat['level{0}_residual_conv_f'.format(level+1)]).unsqueeze(3).permute(3,2,0,1)
            self.residual_layers[level].bias.data = torch.from_numpy(
                mat['level{0}_residual_conv_b'.format(level+1)]).view(-1)
            self.image_upsamples[level].weight.data = torch.from_numpy(
                mat['level{0}_img_up_f'.format(level+1)]).unsqueeze(2).unsqueeze(3).permute(3,2,0,1)
            
            
class L1_Charbonnier_loss(nn.Module):
    """L1 Charbonnierloss."""
    def __init__(self):
        super(L1_Charbonnier_loss, self).__init__()
        self.eps = 1e-6

    def forward(self, X, Y):
        diff = torch.add(X, -Y)
        error = torch.sqrt( diff * diff + self.eps )
        loss = torch.sum(error) 
        return loss