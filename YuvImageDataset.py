from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import numpy as np
from PIL import Image
import os

def distort_image(img):
    i = np.random.randint(0, img.shape[1])
    j = np.random.randint(0, img.shape[2])
    for c in range(img.shape[0]):
        img[c, i, j] = 1

class YuvImageDataset(Dataset):
    def __init__(self, root_dirs, upsample_levels = 2, lowest_size=32, do_distortion=False):
        self.filenames = []
        for root_dir in root_dirs:
            for f in os.listdir(root_dir):
                filename = os.path.join(root_dir, f)
                img = Image.open(filename)
                if min(img.size[0], img.size[1]) >= lowest_size * 2**upsample_levels:
                    self.filenames.append(filename)

        self.preprocess = transforms.Compose([
            transforms.RandomHorizontalFlip(),
            transforms.RandomCrop(lowest_size * 2**upsample_levels),
        ])
        self.resizes = [transforms.Resize(lowest_size * 2**level) for level in range(upsample_levels)]
        self.to_tensor = transforms.ToTensor()
        self.levels = upsample_levels
        self.lowest_size = lowest_size
        self.do_distortion = do_distortion

    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, idx):
        img_name = self.filenames[idx]
        img = Image.open(img_name)
        random_resize = transforms.Resize(np.random.randint(
            self.lowest_size * 2**self.levels, min(img.size[0], img.size[1]) + 1))
        augmented_img = self.preprocess(random_resize(img))
        scaled_images = [resize(augmented_img) for resize in self.resizes]
        scaled_images.append(augmented_img)

        a = dict()
        a['yuv_input_img'] = self.to_tensor(scaled_images[0].convert('YCbCr'))
        if self.do_distortion:
            distort_image(a['yuv_input_img'])
        for i in range(self.levels):
            a['hr_reference_' + str(i+1)] = self.to_tensor(scaled_images[i+1])
            a['y_hr_reference_' + str(i+1)] = self.to_tensor(scaled_images[i+1].convert('YCbCr'))[0, :, :].unsqueeze(0)

        return a