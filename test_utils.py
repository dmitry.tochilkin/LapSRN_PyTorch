import torch
from torch.autograd import Variable
from torchvision import transforms, utils

import LapSRN
import ImageDataset
import YuvImageDataset
from skimage.color import rgb2yuv
import torch.nn as nn
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
import math

def PSNR_torch(im1, im2):
    PIXEL_MAX = 1.0
    mse = torch.mean((im1 - im2) ** 2)
    return 20 * math.log10(PIXEL_MAX / math.sqrt(mse))

def PSNR_np(pred, gt, shave_border=0):
    height, width = pred.shape[:2]
    pred = pred[shave_border:height - shave_border, shave_border:width - shave_border]
    gt = gt[shave_border:height - shave_border, shave_border:width - shave_border]
    imdff = pred - gt
    rmse = math.sqrt(np.mean(imdff ** 2))
    if rmse == 0:
        return 100
    return 20 * math.log10(255.0 / rmse)

def calculate_PSNR(models, test_dataset, metrics_function=PSNR_np, scale=4, n_samples=np.inf, cuda=True):
    metrics = np.zeros(len(models))
    for i in range(min(test_dataset.__len__(), n_samples)):
        sample = test_dataset.__getitem__(i)
        if cuda:
            input_img = Variable(sample['yuv_input_img'][0, :, :].unsqueeze(0).unsqueeze(0).cuda())
        else:
            input_img = Variable(sample['yuv_input_img'][0, :, :].unsqueeze(0).unsqueeze(0))
        level = math.ceil(math.log2(scale)) - 1
        hr_ref = sample['y_hr_reference_'+str(level+1)].numpy() * 255.
        for m in range(len(models)):
            outputs = models[m](input_img)
            hr_y = outputs[level].data.squeeze(0).cpu().numpy().astype(np.float32)
            hr_y = np.clip(hr_y * 255., 0, 255.)        
            metrics[m] += metrics_function(hr_y, hr_ref)
    return metrics / min(test_dataset.__len__(), n_samples)

def eval_on_valset(val_dataset, model, metrics_function=psnr, n_samples=20, cuda=True):
    metrics = 0
    for i in range(min(val_dataset.__len__(), n_samples)):
        sample = val_dataset.__getitem__(i)
        if cuda:
            input_img = Variable(sample['input_img'].unsqueeze(0).cuda())
        else:
            input_img = Variable(sample['input_img'].unsqueeze(0))
        optimizer.zero_grad()   # zero the gradient buffers
        outputs = model(input_img)
        for level in range(len(outputs)):
            hr_ref = sample['hr_reference_'+str(level+1)]
            if cuda:
                hr_ref=hr_ref.cuda()
            metrics += metrics_function(outputs[level].data.squeeze(0), hr_ref)
    return metrics / min(val_dataset.__len__(), n_samples)

def rgb2ycbcr(im):
    xform = np.array([[.299, .587, .114], [-.1687, -.3313, .5], [.5, -.4187, -.0813]])
    ycbcr = im.dot(xform.T)
    ycbcr[:,:,[1,2]] += 128
    return np.uint8(ycbcr)

def ycbcr2rgb(im):
    xform = np.array([[1, 0, 1.402], [1, -0.34414, -.71414], [1, 1.772, 0]])
    rgb = im.astype(np.float)
    rgb[:,:,[1,2]] -= 128
    return np.uint8(np.clip(rgb.dot(xform.T), 0, 255))

def get_upscaled_rbg(yuv_img_tensor, hr_y, scale_factor, upsampling=Image.BICUBIC):
    ''' Given a low resolution image tensor in YUV colormap and a high-resolution Y-channel (luminance), returns
        a high-resolution rgb image by combining the high-res Y-channel and result of upscaling U and V channels of
        the low-res image.
    '''
    yuv_img = transforms.ToPILImage()(yuv_img_tensor)
    w, h = yuv_img.size
    w, h = w*scale_factor, h*scale_factor
    upscaled_yuv = yuv_img.resize((w, h), upsampling)
    upscaled_yuv_tensor = transforms.ToTensor()(upscaled_yuv)
    upscaled_yuv_tensor[0,:,:] = hr_y
    upscaled_yuv = transforms.ToPILImage()(upscaled_yuv_tensor)
    return ycbcr2rgb(np.array(upscaled_yuv))

def upscale_lapSRN(sample, lap_srn):
    ''' Given a low resolution YUV image sample, computes high-resolution versions of it by means of
        a trained LapSRN network.
    '''
    outputs = lap_srn(Variable(sample['yuv_input_img'][0, :, :].unsqueeze(0).unsqueeze(0).cuda()))
    levels = len(outputs)
    rgb_images = []
    for level in range(levels):
        rgb_images.append(get_upscaled_rbg(
            sample['yuv_input_img'], torch.clamp(outputs[level].data.cpu(), 0, 1).squeeze(0), 2**(level+1)))
    return rgb_images

def compare_results(lapsrn, sample, pretrained_lapsrn=None):
    levels = len(sample) // 2
#     outputs = lapsrn(Variable(sample['yuv_input_img'].unsqueeze(0).cuda()))
    outputs = lapsrn(Variable(sample['yuv_input_img'][0, :, :].unsqueeze(0).unsqueeze(0).cuda()))
    fig = plt.figure(figsize=(8,16))
    rows = 3
    if pretrained_lapsrn is not None:
        rows = 4
        outputs_pretrained = pretrained_lapsrn(Variable(sample['yuv_input_img'][0, :, :].unsqueeze(0).unsqueeze(0).cuda()))
    
    for level in range(levels):
        ax = plt.subplot(rows, levels, 1+level)
        plt.tight_layout()
        ax.set_title('bilinear_uapsample_{}'.format(level+1))
        upscale = nn.Upsample(scale_factor=2**(level+1), mode='bilinear')
        plt.imshow(ycbcr2rgb(np.array(transforms.ToPILImage()(
            upscale(Variable(sample['yuv_input_img'].unsqueeze(0))).data.squeeze(0)))))
        
        if pretrained_lapsrn is not None:
            ax = plt.subplot(rows, levels, 3+level)
            plt.tight_layout()
            ax.set_title('output_pretrained_{}'.format(level+1))
    #         plt.imshow(transforms.ToPILImage()(torch.clamp(outputs[level].data.cpu(), 0, 1).squeeze(0)))
            plt.imshow(get_upscaled_rbg(
                sample['yuv_input_img'], torch.clamp(outputs_pretrained[level].data.cpu(), 0, 1).squeeze(0), 2**(level+1)))
        
        ax = plt.subplot(rows, levels, level + (3 if pretrained_lapsrn is None else 5))
        plt.tight_layout()
        ax.set_title('my_output_{}'.format(level+1))
#         plt.imshow(transforms.ToPILImage()(torch.clamp(outputs[level].data.cpu(), 0, 1).squeeze(0)))
        plt.imshow(get_upscaled_rbg(
            sample['yuv_input_img'], torch.clamp(outputs[level].data.cpu(), 0, 1).squeeze(0), 2**(level+1)))

        ax = plt.subplot(rows, levels, level + (5 if pretrained_lapsrn is None else 7))
        plt.tight_layout()
        ax.set_title('hr_reference_{}'.format(level+1))
        plt.imshow(transforms.ToPILImage()(sample['hr_reference_'+str(level+1)]))